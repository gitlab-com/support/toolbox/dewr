# frozen_string_literal: true

# Dotcom Escalation Weekly Report module
module DEWR
  require 'bundler/setup'
  Bundler.require(:default)
  require_relative 'dewr/client'
end
