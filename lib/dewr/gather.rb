# frozen_string_literal: true

# Dotcom Escalation Weekly Report module
module DEWR
  # Gather class
  class Gather < Client
    def self.run!
      artifact
    end

    def self.data
      @data ||= {
        current: gather_recent_issues,
        outstanding: gather_open_issues
      }
    end

    def self.artifact
      open('data/issues.json', 'w') { |f| f.puts data.to_json }
    end

    def self.gather_data
      list = []
      gather_issues.each do |i|
        created_at = Date.parse(i['created_at'])
        list.push(i) if created_at <= (Date.today - 20)
      end
      list
    end

    def self.labels
      ['Admin Escalation', 'Console Escalation::GitLab.com']
    end

    def self.gather_recent_issues
      opts = "created_after=#{start_date}&per_page=100&order_by=created_at&sort=asc"
      issues = request(:get, "/projects/7083173/issues?#{opts}")
      issues.select { |i| i['labels'].any? { |l| labels.include? l } }
    end

    def self.gather_open_issues
      opts = "created_before=#{start_date}&state=opened&per_page=100&order_by=created_at&sort=asc"
      issues = request(:get, "/projects/7083173/issues?#{opts}")
      issues.select { |i| i['labels'].any? { |l| labels.include? l } }
    end
  end
end
