# frozen_string_literal: true

# Dotcom Escalation Weekly Report module
module DEWR
  # Create class
  class Create < Client
    def self.run!
      create_issue
    end

    def self.data
      @data ||= JSON.parse(File.read('data/issues.json'))
    end

    def self.title
      "Dotcom Escalations Report for: #{start_date} - #{end_date}"
    end

    def self.message
      erb_renderer = ERB.new(File.read('data/template.erb'))
      erb_renderer.result_with_hash(
        current_issues: data['current'],
        outstanding_issues: data['outstanding']
      )
    end

    def self.opts
      {
        title: title,
        description: message
      }
    end

    def self.create_issue
      pp request(:post, '/projects/7083173/issues', opts)
    end
  end
end
