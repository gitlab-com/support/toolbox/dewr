# frozen_string_literal: true

# Dotcom Escalation Weekly Report module
module DEWR
  # Client class
  class Client
    require_relative 'create'
    require_relative 'gather'

    def self.retry_options
      {
        max: 5,
        interval: 1,
        interval_randomness: 0.5,
        backoff_factor: 2,
        exceptions: [Faraday::ConnectionFailed, Faraday::TimeoutError]
      }
    end

    def self.gitlab
      @gitlab ||= Faraday.new('https://gitlab.com/api/v4') do |c|
        c.request :retry, retry_options
        c.request :url_encoded
        c.adapter Faraday.default_adapter
        c.headers['Private-Token'] = ENV.fetch('GL_TOKEN')
      end
    end

    def self.redis_client
      # Sets the variable client to an instance of the Redis class, passing the
      # parameters of:
      #   host: ENV.fetch('REDIS_HOST', nil)
      #     Use the value of the environment variable REDIS_HOST
      #   port: ENV.fetch('REDIS_PORT', nil)
      #     Use the value of the environment variable REDIS_PORT
      #   password: ENV.fetch('REDIS_PASSWORD', nil)
      #     Use the value of the environment variable REDIS_PASSWORD
      #   ssl: true
      #     Signals to use TLS for the connection
      @redis_client ||= ::Redis.new(host: ENV.fetch('REDIS_HOST', nil),
                                    port: ENV.fetch('REDIS_PORT', nil),
                                    password: ENV.fetch('REDIS_PASSWORD', nil),
                                    ssl: true)
    end

    def self.request(method, url, params = {})
      response = gitlab.public_send(method, "https://gitlab.com/api/v4#{url}", params)
      Oj.load(response.body)
    end

    def self.support_team
      @support_team ||= support_team_yaml
    end

    def self.support_team_yaml
      # Use Marshal to deserialize the return of calling the Redis client to get
      # the value of the key support_team
      Marshal.load redis_client.get('support_team')
    end

    def self.start_date
      return Time.now.strftime('%Y-%m-%d') if Time.now.wday.zero?

      Time.now.prev_occurring(:sunday).strftime('%Y-%m-%d')
    end

    def self.end_date
      return Time.now.strftime('%Y-%m-%d') if Time.now.wday == 6

      Time.now.next_occurring(:saturday).strftime('%Y-%m-%d')
    end
  end
end
