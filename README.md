# DEWR

Dotcom Escalations Weekly Report

## How it works

In one phase of CI/CD, it gathers all issues, selecting only the ones containing
the labels ~"Admin Escalation" or ~"Console Escalation::GitLab.com".

In the next phase, it then uses the template.erb file to generate the issue
content. Once generated, it then creates the issue.

## How often does this run

This runs every Saturday as 0000 UTC.
